from util import rotate_left, rotate_right
import random

f = open('input.txt', 'r')
input = f.read()

def get_heat_map():
  return input.split('\n')

heat_map = get_heat_map()

destination = [len(heat_map[0]), len(heat_map)]

def get_min_cost_map(): 
  min_cost_map = [[] for _ in range(len(heat_map))]

  for i in range(len(min_cost_map)):
    min_cost_map[i] = [[] for _ in range(len(heat_map[0]))]
    
  for i in range(len(min_cost_map)):
    for j in range(len(min_cost_map[i])):
      min_cost_map[i][j] = 9 * (( i ) + ( j ))

  min_cost_map[0][0] = 0

  return min_cost_map

def read_min_cost_map_from_file():
  f_map = open('cost_map.txt', 'r')
  min_cost_input = f_map.read().split('\n')
  cost_map_from_file = [[] for _ in range(len(min_cost_input))]
  for i in range(len(cost_map_from_file)):
    cost_map_from_file[i] = min_cost_input[i].split(',')
    for j in range(len(cost_map_from_file[i])):
      cost_map_from_file[i][j] = int(cost_map_from_file[i][j])

  return cost_map_from_file

# TODO ? if file does not exist init, else read
# min_cost_map = get_min_cost_map()
min_cost_map = read_min_cost_map_from_file()

# position, direction, cost, straight
initial_move = {"position": [0,0], "direction": [1,0], "cost": 0, "straight": 0, "path": []}

def find_next_moves(move = initial_move):
  x = move["position"][0]
  y = move["position"][1]
  dirX = move["direction"][0]
  dirY = move["direction"][1]

  if x == -1 or y == -1 or x >= len(heat_map[0]) or y >= len(heat_map):
    return []
  
  if min_cost_map[y][x] < move["cost"]:
    return []
  else:
    min_cost_map[y][x] = move["cost"]

  if move["position"] == destination:
    return []

  if move["position"] in move["path"]:
    return []

  next_cost = move["cost"] + int(heat_map[y][x])

  next_moves = []
  next_path = move["path"].copy()
  next_path.append(move["position"])

  # add possible move straight if possible (count<3)
  if move["straight"] < 3:
    next_moves.append({"position": [x + dirX, y + dirY], "direction": move["direction"], "cost": next_cost, "straight": move["straight"] + 1, "path": next_path })

  # add rotate_left
  dirLeft = rotate_left(move["direction"])
  dirLeftX = dirLeft[0]
  dirLeftY = dirLeft[1]
  next_moves.append({"position" : [x + dirLeftX, y + dirLeftY], "direction": dirLeft, "cost": next_cost, "straight": 0, "path": next_path})

  # add rotate_right
  dirRight = rotate_right(move["direction"])
  dirRightX = dirRight[0]
  dirRightY = dirRight[1]
  next_moves.append({"position" : [x + dirRightX, y + dirRightY], "direction": dirRight, "cost": next_cost, "straight": 0, "path": next_path})

  return next_moves

count = 0

def get_cost(move): 
  return move["cost"]

possible_moves = [initial_move]
while len(possible_moves) > 0:
  count += 1
  next_move = possible_moves.pop()

  possible_moves += find_next_moves(next_move)
  if count%10000000 == 0:
    random.shuffle(possible_moves)
    possible_moves.sort(key=get_cost)
    print("--------------")
    print({"moves": len(possible_moves)})
    print({"cost": next_move["cost"]})
    print(next_move["path"])
    print({"path_length": len(next_move["path"])})
    print({"cost to reach end": min_cost_map[len(heat_map[0]) - 1][len(heat_map) - 1]})

    cost_map_output = ''
    for i in range(len(min_cost_map)):
      for j in range(len(min_cost_map[0])):
        cost_map_output += str(min_cost_map[i][j]) + ','
      cost_map_output = cost_map_output[:-1] + '\n'
    cost_map_output = cost_map_output[:-1]
    f_write = open('cost_map.txt', 'w')
    f_write.write(cost_map_output)

cost_map_output = ''
for i in range(len(min_cost_map)):
  for j in range(len(min_cost_map[0])):
    cost_map_output += str(min_cost_map[i][j]) + ','
  cost_map_output = cost_map_output[:-1] + '\n'
cost_map_output = cost_map_output[:-1]

f_write = open('cost_map.txt', 'w')
f_write.write(cost_map_output)

# don't forget to add the last cost 
print(min_cost_map[len(heat_map[0]) - 1][len(heat_map) - 1])
