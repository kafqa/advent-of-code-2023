def rotate_left(directions = [1,0]):
  # [1,0] => [0,-1]
  # [0,-1] => [-1,0]
  # [-1,0] => [0,1]
  # [0,1] => [1,0]
  return [directions[1],-directions[0]]

def rotate_right(directions = [1,0]):
  # [1,0] => [0,1]
  # [0,-1] => [1,0]
  # [-1,0] => [0,-1]
  # [0,1] => [-1,0]
  return [-directions[1],directions[0]]