import sys
sys.setrecursionlimit(1114584500)


f = open('input.txt', 'r')
input = f.read()

fields = input.split('\n')

def energize(energy_map, loop_map, backlog = [], start = [0, 0], direction = [1, 0]):
  x = start[0]
  y = start[1]
  dirX = direction[0]
  dirY = direction[1]

  if x == -1 or y == -1 or x >= len(fields[0]) or y >= len(fields):
    return 
  
  energy_map[y][x] = 1

  if direction in loop_map[y][x]:
    return 
  else:
    loop_map[y][x].append(direction)

  if fields[y][x] == '-' and dirX == 0:
    backlog.append([[x - 1, y],[ - 1, 0]])
    energize(energy_map, loop_map, backlog, [x + 1, y], [1,0])
    return 
  
  if fields[y][x] == '|' and dirY == 0:
    backlog.append([[x, y - 1],[0, -1]])
    energize(energy_map, loop_map, backlog, [x, y + 1], [0,1])
    return

  next = [x + dirX, y + dirY]
  newDirection = direction

  if fields[y][x] == '/':
    # [1,0] => [0,-1]
    # [-1,0] =) [0, 1]
    # [0,1] => [-1,0]
    # [0,-1] => [1,0]
    newDirection = [-dirY, -dirX]
    next = [x + newDirection[0], y + newDirection[1]]

  if fields[y][x] == '\\':
    # [1,0] => [0,1]
    # [-1,0] =) [0, -1]
    # [0,1] => [1,0]
    # [0,-1] => [-1,0]
    newDirection = [dirY, dirX]
    next = [x + newDirection[0], y + newDirection[1]]

  energize(energy_map, loop_map, backlog, next, newDirection)

def energizeWithStart(start = [0, 0], direction = [1, 0]): 

  energy_map = [[] for _ in range(len(fields))]

  for i in range(len(energy_map)):
    energy_map[i] = [[] for _ in range(len(fields[0]))]
    
  for i in range(len(energy_map)):
    for j in range(len(energy_map[i])):
      energy_map[i][j] = 0

  loop_map = [[] for _ in range(len(fields))]

  for i in range(len(loop_map)):
    loop_map[i] = [[] for _ in range(len(fields[0]))]

  for i in range(len(loop_map)):
    for j in range(len(loop_map[i])):
      loop_map[i][j] = []

  backlog = []

  energize(energy_map, loop_map, backlog, start, direction)
  while(True):
    if len(backlog) == 0:
      break
    else:
      old_backlog = backlog
      backlog = []
      for item in old_backlog:
        energize(energy_map, loop_map, backlog, item[0], item[1])

  count = 0
  for row in energy_map:
    for field in row:
      if field == 1:
        count += 1

  return count

maxY = len(fields)
maxX = len(fields[0])

max_value = 0

# left entry
for i in range(maxY):
  fieldsEnergized = energizeWithStart([0,i], [1,0])
  if fieldsEnergized > max_value:
    max_value = fieldsEnergized
    print(max_value)

# bottom entry
for i in range(maxX):
  fieldsEnergized = energizeWithStart([i,maxY ], [0,-1])
  if fieldsEnergized > max_value:
    max_value = fieldsEnergized
    print(max_value)

# top entry
for i in range(maxX):
  fieldsEnergized = energizeWithStart([i,0], [0,1])
  if fieldsEnergized > max_value:
    max_value = fieldsEnergized
    print(max_value)

# right entry
for i in range(maxY):
  fieldsEnergized = energizeWithStart([maxX ,i], [-1,0])
  if fieldsEnergized > max_value:
    max_value = fieldsEnergized
    print(max_value)

print(max_value)

