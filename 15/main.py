
from input import input_array

def hash(input_string):
  current_value  = 0

  for letter in input_string:
    current_value += (ord(letter))
    current_value *= 17

  current_value %= 256
  return current_value

def hash_sequence(input_array):
  sum_value = 0
  for value in input_array:
    sum_value += hash(value)

  return sum_value

def remove_label(box, label):
  new_box_list = []
  for entry in box:
    if not(entry.split('=')[0] == label):
      new_box_list.append(entry)
  return new_box_list

def replace_label(box, label):
  new_box_list = []
  label_name = label[:-2]
  replaced = False

  for entry in box:
    if entry[:-2] == label_name:
      new_box_list.append(label)
      replaced = True
    else:
      new_box_list.append(entry)


  if not(replaced):
    new_box_list.append(label)

  return new_box_list


def sort_into_boxes(input_array): 
  boxes = [[] for _ in range(256)]

  for lens_label in input_array:
    if lens_label[-1:] == '-':
      hash_value = hash(lens_label[:-1])
      boxes[hash_value] = remove_label(boxes[hash_value], lens_label[:-1])
    else:
      hash_value = hash(lens_label[:-2])
      boxes[hash_value] = replace_label(boxes[hash_value], lens_label)



  return boxes 

def evaluate(boxes):
  print(boxes)
  focusing_power = 0

  
  i_box = 1
  for box in boxes:
    i_slot = 1
    for slot in box:
      focusing_power += (i_box) * (i_slot) * int(slot[-1:])
      i_slot += 1
    i_box += 1
  return focusing_power


sorted_boxes = (sort_into_boxes(input_array))


print(evaluate(sorted_boxes))