f = open('input.txt', 'r')
input = f.read().split('\n')

dig_steps = []

for dig_step_string in input:
  dig_step = dig_step_string.split(' ')
  dig_steps.append({"direction": dig_step[0], "length": dig_step[1], "color": dig_step[2]})

def get_max_x():
  max_x = 0
  max_neg_x = 0
  current_x = 0
  for dig_step in dig_steps:
    if dig_step["direction"] == "R":
      current_x += int(dig_step["length"])
      if current_x > max_x:
        max_x = current_x
    if dig_step["direction"] == "L":
      current_x -= int(dig_step["length"])
      if current_x < max_neg_x:
        max_neg_x = current_x
  return max_x - max_neg_x

def get_max_y():
  max_y = 0
  max_neg_y = 0
  current_y = 0
  for dig_step in dig_steps:
    if dig_step["direction"] == "D":
      current_y += int(dig_step["length"])
      if current_y > max_y:
        max_y = current_y
    if dig_step["direction"] == "U":
      current_y -= int(dig_step["length"])
      if current_y < max_neg_y:
        max_neg_y = current_y
  return max_y - max_neg_y

max_x = get_max_x()
max_y = get_max_y()

def initialize_map():
  map = [[] for _ in range(max_y + 1)]

  for i in range(len(map)):
    map[i] = [[] for _ in range(max_x + 1)]

  for i in range(len(map)):
    for j in range(len(map[i])):
      map[i][j] = '.'

  return map

trench_map = initialize_map()

def add_to_direction(direction, length):
  move_step = [0,0]
  if direction == 'R':
    move_step[0] = length
  if direction == 'L':
    move_step[0] = -length
  if direction == 'D':
    move_step[1] = length
  if direction == 'U':
    move_step[1] = -length

  return move_step

def dig_trenches():
  position = [0,0]
  for step in dig_steps:
    direction = step["direction"]
    length = int(step["length"])

    for _ in range(length):
      move_one = add_to_direction(direction, 1)
      position = [position[0] + move_one[0], position[1] + move_one[1]]
      trench_map[position[1]][position[0]] = step["color"]


dig_trenches()
  
full_dig_map = initialize_map()

def paint(): 
  for i in range(len(full_dig_map)):
    previous_field = '.'
    painting = False
    
    for j in range(len(full_dig_map[i])):
      if trench_map[i][j] != '.':
        full_dig_map[i][j] = 'X'

      if painting and trench_map[i][j] == '.':
        full_dig_map[i][j] = '#'

      if trench_map[i][j] != '.' and previous_field == '.':
        painting = not(painting)

      previous_field = trench_map[i][j]
  
paint()

count_dig_area = 0

for i in range(len(full_dig_map)):
  for j in range(len(full_dig_map[i])):
    if full_dig_map[i][j] != '.':
      count_dig_area += 1

print(count_dig_area)